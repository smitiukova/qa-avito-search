import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RunWith(JUnit4.class)
public class GetPhonesInfo {

    private WebDriver driver;
    private List<Phone> phonesInfo;

    private final By item = By.className("iva-item-body-KLUuy");
    private final By title = By.className("iva-item-titleStep-pdebR");
    private final By price = By.className("iva-item-priceStep-uq2CQ");
    private final By link = By.className("iva-item-titleStep-pdebR");

    @Before
    public void setUp() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @Test
    public void searchForProducts() throws IOException {
        driver.get("https://www.avito.ru");
        MainPage mainPage = new MainPage(driver);
        mainPage.selectCity("Москва");
        mainPage.searchForProduct("iPhone XS 256gb");
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);
        searchResultsPage.selectOrderingByDate();
        phonesInfo = getPhonesInfo();
        sortByPrice();
        writeToFile();
    }

    private List<Phone> getPhonesInfo() {
        List<WebElement> first10Phones = driver.findElements(item).subList(0, 10);
        List<Phone> phonesInfo = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Phone p = new Phone();
            WebElement element = first10Phones.get(i);
            p.setName(element.findElement(title).getText());
            p.setPrice(new BigDecimal(removeNotNumbers(element.findElement(price).getText())));
            p.setLink(element.findElement(link).findElement(By.cssSelector("a")).getAttribute("href"));
            phonesInfo.add(p);
        }
        return phonesInfo;
    }

    private String removeNotNumbers(String price){
        return price.replace(" ", "").replace("\u20BD", "");
    }

    private void sortByPrice() {
        phonesInfo.sort(Comparator.comparing(Phone::getPrice));
    }

    private void writeToFile() throws IOException {
        try (FileWriter writer = new FileWriter("output.txt")) {
            for (Phone phone : phonesInfo) {
                writer.write(phone.toString() + System.lineSeparator());
            }
        }
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}