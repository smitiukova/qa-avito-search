import java.math.BigDecimal;

public class Phone {
    private String name;
    private BigDecimal price;
    private String link;

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", link='" + link + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
