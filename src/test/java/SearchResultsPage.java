import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class SearchResultsPage {

    private final WebDriver driver;

    private final By sorting = By.className("select-select-IdfiC");

    SearchResultsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectOrderingByDate() {
        List<WebElement> sortingOptions = driver.findElements(sorting);
        WebElement scroll = sortingOptions.get(1);
        scroll.click();
        Select options = new Select(scroll);
        List<WebElement> sortingType = options.getOptions();
        sortingType.get(3).click();
    }
}