import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class MainPage {

    private final WebDriver driver;

    private final By citySelect = By.className("main-text-_Thor");
    private final By cityInput = By.className("suggest-input-rORJM");
    private final By confirmSearchByCity =
            By.xpath("/html/body/div[1]/div[2]/div/div[2]/div/div[6]/div/div/span/div/div[3]/div/div/div/button");
    private final By searchForm = By.className("input-input-Zpzc1");
    private final By confirmProductSearch = By.className("form-part-button-qO9Yf");

    MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public void selectCity(String city) {
        driver.findElement(citySelect).click();
        driver.findElement(cityInput).sendKeys(city);
        driver.findElement(confirmSearchByCity).click();
    }

    public void searchForProduct(String product) {
        driver.findElement(searchForm).sendKeys(product);
        driver.findElement(confirmProductSearch).click();
    }
}
